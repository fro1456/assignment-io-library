section .text


; Принимает код возврата и завершает текущий процесс
exit:
    mov    rax, 60; системный вызов 60 это выход
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax; обнуление строки
    push rdi ;закинули начало строки

    .cnt:
        cmp byte [rdi],0
        je  .suc
        inc rdi
        jmp .cnt

    .suc:
        mov rax,rdi ;сохранили текущее значение с rdi
        pop rdi ;вернули начало
        sub rax,rdi ;вычитаем начало из того что получили
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi;так как это caller-saved
    call string_length;возвращаю длину строки
    mov rdx,rax;сохраняю длину
    mov rsi,rdi; сохраняю данные rdi в rsi
    mov rdi,1;stdout
    mov rax,1;syscall на write
    syscall
    pop rdi


    ret




; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi,0xA ;записываем перенос строки (тот самый 0xA)
print_char: ; Принимает код символа и выводит его в stdout
    push rdi
    mov rdi,rsp
    call print_string
    pop rdi
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax,rdi;вот он источник моих проблем
    ;так как оно вроде заработало, считаю нужным написать комменты

    mov r9, 10;это то число на которое мы делим
    push 0

    .separation:
        mov rdx, 0
        div r9 ; деление на константу
        add rdx, 0x30 ; так как число ASCII, мы добавляем 0
        push rdx ; кидаю число в стек
        cmp rax,0 ; смотрю не кончилось ли все
        je .print_symbol ; кончилось, значит печатаем
        jmp .separation ; не кончилось, значит дальше делим
    .print_symbol:
        pop rdi ; достаем из стека
        cmp rdi,0
        je .ex ; прыгаю на выход если конец
        call print_char ; (вызываю функцию которую сделал еще тогда, просто вывод символа)
        jmp .print_symbol ; возвращаюсь в print_symbol

    .ex:
        ret



; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi,0
    js .neg
    .pos:
        call print_uint
        ret
    .neg:
        push rdi
        mov rdi,'-'
        call print_char
        pop rdi
        neg rdi
        jmp print_uint




string_equals:
    xor r8,r8
    .check:
        mov r9b, byte[r8+rdi]
        mov r10b, byte[r8+rsi]
        inc r8
        cmp r9b,r10b
        jne .not_eq
        cmp r10b,0
        je .eq
        jmp .check

    .not_eq:
        mov rax, 0
        ret
    .eq:
        mov rax, 1
        ret
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi,rdi
    mov rdx,1 ;читаем
    push 0
    mov rsi,rsp
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx,rdx
    .chk:
        push rdi
        push rsi
        push rdx

        call read_char

        pop rdx
        pop rsi
        pop rdi

        cmp rax,0
        je .suc

        ;необходимо проверить на наличие пробелов в начале
        cmp rax,0x20
        je .spchk
        cmp rax,0xa
        je .spchk
        cmp rax,0x9
        je .spchk
        jmp .rd

        .spchk:
            cmp rdx, 0
            je .chk
            jmp .suc

        .rd:
            mov byte[rdi+rdx], al
            inc rdx
            jmp .chk
        .suc:
            mov byte[rdi+rdx],0 ; нуль-терминант
            mov rax,rdi
            ret
        .fail:
            xor rax,rax
            ret




    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi,rsi
    xor rdx,rdx
    xor r9,r9
    mov r10,10
    .cnt:
        mov sil, byte[rdi+r9]
        cmp sil,'0'
        jl .ex
        cmp sil,'9'
        jg .ex
        sub sil,'0'
        ;push rdx
        mul r10
        ;pop rdx
        add rax, rsi
        inc r9
        xor rsi, rsi
        jmp .cnt
    .ex:
        mov rdx, r9
        ret

    mov rdx, r9
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax,rax
    cmp byte[rdi],'-'
    je .neg
    call parse_uint
    jmp .ext
    .neg:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
    .ext
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r10,r10
    call string_length
    inc rax
    cmp rdx,rax
    jl .less
    .next:
        mov r10b,byte[rdi+r11]
        mov byte[rsi+r11],r10b
        inc r11
        cmp r11,rax
        jl .next
        dec rax



    .less:
        xor rax,rax
        ret
